<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Prabu Widget Example</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- Bootstrap CSS -->
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    />
  </head>
  <body>
    <div>
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h5>Test Embed Script</h5>
          <hr class="my-2" />
          <form class="js-add-code-form">
            <div class="form-group mb-0">
              <label for=""
                >Past copied code here to insert the code to the page.</label
              >
              <textarea
                class="form-control js-code-field"
                name=""
                rows="2"
                required
              ></textarea>
            </div>
            <div class="mb-3">
                <small class="text-muted"
                  >Note: usually the code will be hardcoded.</small
                >
            </div>
            <div>
              <button type="submit" class="btn btn-primary js-insert">
                Insert
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <script>
      const insertFormEl = document.querySelector(".js-add-code-form");
      insertFormEl.addEventListener("submit", (e) => {
        e.preventDefault();
        const form = e.target;
        Array.from(form.elements).forEach((el) => (el.disabled = true));
        const codeEl = document
          .querySelector(".js-code-field")
          .value.match(/src\=\"(.+)"/)[1];
        let tempElem = document.createElement("script");
        tempElem.src = codeEl;
        document
          .getElementsByTagName("body")[0]
          .insertAdjacentElement("beforeend", tempElem);
      });
    </script>
  </body>
</html>
