<?php

    $effectId  = '';
    $client_custom_scripts_header = '';
    $client_custom_scripts_bottom = '';
    $client_effect_start_date = '';
    $client_effect_end_date = '';
    $client_time_zone = '';

        
    $seconds_to_cache = (3600 * 24 ) * 7;
    // $seconds_to_cache = 3600 * 2;
    $ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
    header("Expires: $ts");
    header("Pragma: cache");
    header("Cache-Control: max-age=$seconds_to_cache");
    header("content-type: text/javascript");


    $jQAdd    = 0;
    if (isset($_GET['jq']) && $_GET['jq'] == 1) {
        $jQAdd = 1;
    }

    $effectIds = explode(',', $effectId);

    $custom_scripts_header = file_get_contents('effect-html-mini.html');
    $custom_scripts_bottom = file_get_contents('effect-scripts-mini.html');;

    ob_start();

    ?>
var domReady = function(p) {
var callback = p || function() {};
if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
callback();
} else {
document.addEventListener("DOMContentLoaded", callback);
}
};
document.getElementsByTagName('html')[0].style.display = 'none';
document.getElementsByTagName('html')[0].style.visibility = 'hidden';
document.getElementsByTagName('body')[0].addEventListener("load", function(){
var $scripts = document.getElementsByTagName('script');

for (var i = 0; i < $scripts.length; i++) { $scripts[i].removeAttribute('async') } }); domReady(function () { var
    $tempHtmls=document.getElementsByTagName('html'); for (var i=0; i < $tempHtmls.length; i++) { if
    ($tempHtmls[i].childNodes.length < 1) { $tempHtmls[i].outerHTML="" ; delete $tempHtmls[i]; } } }); var jQAdd=<?php echo $jQAdd; ?>;
    var jQLink = 'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js';
    var headerScripts = <?php echo "'". base64_encode($custom_scripts_header) . "'"; ?>;
    var footerScripts = <?php echo "'".  base64_encode($custom_scripts_bottom) . "'"; ?>;
    var tempElem = document.createElement('span');

    tempElem.innerHTML = atob(footerScripts) + atob(headerScripts);

    var resourceInjector = function (documentObj, element, link, html, traget) {

    var tempElem = documentObj.createElement(element);
    var traget = 'afterbegin';
    if (element == 'script') {
    traget = 'beforeend';
    if (link !== jQLink) {
    tempElem.async = false;
    }
    if (jQAdd !== 1 && link == jQLink) {
    return false;
    }
    if (jQAdd == 1 && link == jQLink) {
    tempElem.async = false;

    }
    if (link) {
    tempElem.src = link;
    tempElem.className = "js-resource-links";
    }
    if (html) {
    tempElem.innerHTML = html;
    }
    }
    if (element == 'link') {
    tempElem.href = link;
    tempElem.rel = 'stylesheet';
    }
    if (element == 'style') {
    traget = 'beforeend';
    if (html) {
    tempElem.innerHTML = html;
    }
    }
    if (element == 'html') {
    traget = 'beforeend';
    document.getElementsByTagName('body')[0].insertAdjacentElement(traget, html);
    }
    if (traget) {
    var tempTraget = documentObj.getElementsByTagName('body')[0];
    if (element == 'script') {
    if (jQAdd == 1) {
    if (jQAdd == 1 && link == jQLink) {
    tempElem.id = "jQSrcId";
    if (link) {
    tempTraget.insertAdjacentElement(traget, tempElem);
    } else {
    domReady(function () {
    // setTimeout(function(){
    tempTraget.insertAdjacentElement(traget, tempElem);
    // }, 3000);
    });

    }
    } else {
    var tempScrLength = document.getElementsByClassName('js-resource-links');
    if (tempScrLength.length > 0) {
    tempScrLength[tempScrLength.length - 1].addEventListener("load", function () {
    // domReady(function() {
    // setTimeout(function(){
    if (link) {
    tempTraget.insertAdjacentElement(traget, tempElem);
    } else {
    domReady(function () {
    // setTimeout(function(){
    tempTraget.insertAdjacentElement(traget, tempElem);
    // }, 3000);
    });

    }
    // }, 3000);
    // });

    });
    } else {
    if (link) {
    tempTraget.insertAdjacentElement(traget, tempElem);
    } else {
    domReady(function () {
    // setTimeout(function(){
    tempTraget.insertAdjacentElement(traget, tempElem);
    // }, 3000);
    });

    }
    }


    }
    } else {

    var tempScrLength = document.getElementsByClassName('js-resource-links');
    console.log(tempScrLength.length);
    if (tempScrLength.length > 0) {
    tempScrLength[tempScrLength.length - 1].addEventListener("load", function () {
    if (link) {
    tempTraget.insertAdjacentElement(traget, tempElem);
    } else {
    domReady(function () {
    // setTimeout(function(){
    tempTraget.insertAdjacentElement(traget, tempElem);
    // }, 3000);
    });

    }


    });
    } else {
    if (link) {
    tempTraget.insertAdjacentElement(traget, tempElem);
    } else {
    domReady(function () {
    // setTimeout(function(){
    tempTraget.insertAdjacentElement(traget, tempElem);
    // }, 3000);
    });

    }
    }


    }
    } else {
    tempTraget.insertAdjacentElement(traget, tempElem);
    }
    }
    }

    var tempElem = document.createElement('span');
    tempElem.innerHTML = atob(headerScripts) + atob(footerScripts);
    if (jQAdd == 1) {
    resourceInjector(document, 'script', jQLink, null, null);
    }

    for (var i = 0; i < tempElem.children.length; i++) { if (tempElem.children[i].tagName=="SCRIPT" ) { if
        (tempElem.children[i].src !=='' ) { resourceInjector(document, 'script' , tempElem.children[i].src, null, null);
        } else { resourceInjector(document, 'script' , null, tempElem.children[i].innerHTML, null); } } else if
        (tempElem.children[i].tagName=="LINK" ) { tempElem.children[i].tagName; resourceInjector(document, 'link' ,
        tempElem.children[i].href, null, null); } else if (tempElem.children[i].tagName=="STYLE" ) {
        tempElem.children[i].tagName; resourceInjector(document, 'style' , null, tempElem.children[i].innerHTML, null);
        } else { var tempElemClone=tempElem.children[i].cloneNode(true); resourceInjector(document, 'html' , null,
        tempElemClone, null); } } document.getElementsByTagName('html')[0].style.display='block' ;
        document.getElementsByTagName('html')[0].style.visibility='visible' ; 
        
        <?php
echo ob_get_clean();
